const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const AWS = require('aws-sdk');
const request = require('request');
const jwkToPem = require('jwk-to-pem');
const jwt = require('jsonwebtoken');
const jwt_decode = require('jwt-decode');
const crypto =require('crypto');
const path=require('path');
//global.fetch = require('node-fetch');
const configCognito = require('./config/congnitoConfig');
var bodyParser = require('body-parser');
const express = require("express");
const axios = require("axios");
const swaggerjSDoc = require('swagger-jsdoc');
const swaggerUI= require('swagger-ui-express');
cors = require('cors');
//var sql = require("mssql/msnodesqlv8")
const pem = require("pem");
const fs = require("fs");


"use strict";

const { async } = require('rxjs');
// Creates an express object
const app = express();
app.use(cors());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

const options={
    definition:{
        openapi :"3.0.0",
        info:{
            title:" Node JS API Cognito for PPG",
            version: '1.0.0'
        },
        servers:[
            {
            url:"http://localhost:3200/"
            }
        ]
    },
    apis:['./app.js']
}
const swaggerSpec = swaggerjSDoc(options);
app.use('/api-doc',swaggerUI.serve,swaggerUI.setup(swaggerSpec))

const port =    8888;
const port1 =    3200;
const host = '0.0.0.0';
let credential={
  "region": 'ap-southeast-1', // region also you can change
  "accessKeyId": 'AKIAZQLZAMKPGTMKUSH3',
  "secretAccessKey": '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'
 }
 var pool_region = 'ap-southeast-1';
 var UserPoolId='ap-southeast-1_IfX8ZREvN';
 var ClientId='4djq0t39lb0v2is9acehosdhkk';
const poolData={
  UserPoolId,
  ClientId
}
const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.post("/api/cognito/integration",cors(), (req, res,next) => {
 
try{

  const path = 'D:/Plaza/intigrationJsonfileformat/';
  const fs = require('fs');
// dynamic read  directory inject path and class name in constructor
fs.readdir(path, (err, files) => {
  files.forEach(file => {
    if(file!=="nodejs-intigration")
    {    
     var intigration = require("./intigration/intigration")(file,path+file+"/"+file+".json");
      intigration.intigration();
    }
    
  });
  res.send({statusCode:200,message:"Record inserted"})
});

}
catch (e) {
  res.send({statusCode:400,message:e.message})}

});

function trim(str) {
      
  // Use trim() function
  if(typeof str === "string")
  {
    if(str!==undefined && str!==null && str!=='')
    {
    return str.toString().trim();    
    }
    else
    {
      return '';
    }
  }
  else
  {
    return str;
  }
 return ''
  
}
    //  deleteUser user 
    app.post("/", (req, res,next) => {
        debugger;       
     
        const userPool = new AmazonCognitoIdentity.CognitoUserPool(configCognito.poolData);
        var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
          Username: req.query.username,
          Password: req.query.password,
      });
      var userData = {
          Username: req.query.username,
          Pool: userPool
      };
      var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
      cognitoUser.authenticateUser(authenticationDetails, {
          onSuccess: function (result) {
              // console.log(result);
              cognitoUser.deleteUser((err, result) => {
                  if (err) {
                    res.send(err);
                  } else {
                      // console.log("Successfully deleted the user.");
                      res.send(result);
                  }
              });
          },
          onFailure: function (err) {
            res.send(err);
          },
      });
    
      
        });

app.post("/api/cognito/getUserByLogin",cors(), (req, res,next) => { 
                      
         console.log(user);

         let user=jwt_decode(req.query.token)
         console.log(user);
         return res.send(user)            
        });

  // get User Cognito user List
  app.post("/api/cognito/getUserCognito",cors(), (req, res,next) => {
    debugger;
     var params = {
      UserPoolId:configCognito.UserPoolId.userPoolId,
    };
    return new Promise((resolve, reject) => {
      AWS.config.update(configCognito.credential);
      var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
      cognitoidentityserviceprovider.listUsers(params, (err, data) => {
          if (err) {
              res.send(err)
          }
          else {              
              res.send(data.Users.filter((item)=>item.Enabled===true))
          }
      })
    });    
  
    });

    // user Sync in Cognito to ppg-lms 
  app.post("/api/cognito/userSync",cors(), (req, res,next) => {
    debugger;
    
    const datatimestap = JSON.parse(fs.readFileSync("./config/data.json"));
    const datatimestapa = JSON.parse(fs.readFileSync("./config/data.json"));
   


     var params = {
      UserPoolId:configCognito.UserPoolId.userPoolId,
    };
    return new Promise((resolve, reject) => {
      AWS.config.update(configCognito.credential);
      var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
      cognitoidentityserviceprovider.listUsers(params, (err, data) => {
          if (err) {
              res.send(err)
          }
          else {              
              res.send(data.Users.filter((item)=>
              {
                  if(item.UserStatus=== "CONFIRMED")
                  {
             
                    var unixTimestamp = Math.floor(new Date(item.UserCreateDate).getTime()/1000);
                    item.unixTimestamp=unixTimestamp;                    
                    if(unixTimestamp>=datatimestapa.date)
                    {
                       // datatimestap.date = Math.floor(new Date("2022-03-15 00:00:00.000").getTime()/1000);  
                       datatimestap.date = Math.floor(new Date().getTime()/1000);  
                        fs.writeFileSync("./config/data.json", JSON.stringify(datatimestap, null, 4));
                        syncUserDataInsert(item)
                        return item;
                    } 
                  }
              }
              ));

          }
      })
    });    
  
    });

async function syncUserDataInsert(usereData)
{

const body={
    name:usereData.Username,
    mobile:"1234567890",
    email:"abc@plaza.com",
    remoteUserID:usereData.Attributes[0].Value,
    type:"1",
    defaultProperty:await synclookupCodeDataInsert(),
    businessLine:"cc",
    isActive:"true"
};
console.log(body);
let res = await axios({
        method:"POST",
        url : "https://market-api.com:2000/api/master/user/create/user",
        headers: {
            'Content-Type': 'application/json',
            },
            data:body
    });

    let data = res.data;
    console.log(data);

}

async function synclookupCodeDataInsert()
{

const body={
    lookupCode: "DEFAULT_LOUNGE"
};
console.log(body);
let res = await axios({
        method:"POST",
        url : "https://market-api.com:2000/api/master/globalSetting/fetch/lookupCode",
        headers: {
            'Content-Type': 'application/json',
            },
            data:body
    });

    let data = res.data;
    console.log('synclookupCodeDataInsert',data);
    return data.result.values[0].key1;
}

    async function login(username, password) {
        try {
            const cognito = new AWS.CognitoIdentityServiceProvider()
            const pram={
                AuthFlow:'USER_PASSWORD_AUTH',
                ClientId: '642umqjmf5pcevmnqrmpedja1k',
                AuthParameters: {
                  USERNAME: username,    
                  PASSWORD: password,
                  SCRRET_HASH:genratHash(username),
                }
            }; 
            const data=cognito.initiateAuth(pram).promise();
          // console.log(data);
          return data;

          } catch (err) {
            throw err
          }
      }

      function genratHash(username)
      {
          return crypto.createHmac('SHA256','19ihm6lb1h9q84u1aovqr1a6c65ejh8ar6cmhld44uo5r5f6vqti').update(username+'642umqjmf5pcevmnqrmpedja1k').digest('base64');
      }
    

/**
 * @swagger
 *  components:
 *      schema:
 *          loginUser:
 *                        type: object
 *                        properties:
 *                              username:
 *                                      type: string
 *                              password:
 *                                      type: string
 */

/**
 * @swagger
 *  components:
 *      schema:
 *          loginUserResponse:
 *                        type: object
 *                        properties:
 *                              statusCode:
 *                                         type: string
 *                              input:
 *                                    type: string
 *                              tag:
 *                                  type: string
 *                              status:
 *                                    type: string
 *                              description: 
 *                                         type: string
 *                              result:                                          
 *                                    type: object
 *                                    required: [idToken, refreshToken, accessToken, clockDrift]
 *                                    properties:
 *                                           idToken:
 *                                                  type: string
 *                                           refreshToken:
 *                                                       type: string
 *                                           accessToken:
 *                                                      type: string
 *                                           clockDrift:
 *                                                     type: integer
 */


/**
 * @swagger
 *  components:
 *      schema:
 *          updateUser:
 *                  type: object
 *                  properties:
 *                        username:
 *                                 type: string
 *                        name:
 *                            type: string
 *                        portlocation:
 *                                     type: string
 *                        emailid:
 *                               type: string
 *                        empid:
 *                             type: string
 *                        dob:
 *                            type: string
 *                        role:
 *                             type: string
 *                        approvedby:
 *                                  type: string
 */

/**
* @swagger
* /api/cognito/loginUser:
*   post:
*      summary: This api used for Cognito user login User
*      description: This api used for Cognito user Register
*      requestBody:
*           required: true
*           content:
*                application/json:
*                     schema:
*                          $ref: '#components/schema/loginUser'
*      responses:
*          200:
*              description: User login successful.  
*              content:
*                     application/json:
*                           schema:
*                                  $ref: '#components/schema/loginUserResponse'    
*/
 app.post("/api/cognito/loginUser",cors(), (req, res,next) => {
   debugger;
        const userPool = new AmazonCognitoIdentity.CognitoUserPool(configCognito.poolData);
        var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
          Username: req.body.username,
          Password: req.body.password,
      });
      var userData = {
          Username: req.body.username,
          Pool: userPool
      };
      var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
      cognitoUser.authenticateUser(authenticationDetails, {
          onSuccess: function (result) {     
              debugger;
             // res.send(result);
              res.send(resResult(1,200,"login User successfully",result,"/api/cognito/loginUser"));
          },
          onFailure: function (err) {
            res.send(resResult(1,400,"login User not successfully",err,"/api/cognito/loginUser"));
            res.send(
                {
                    statusCode:400,
                    err
                });
          },
      });  
        
    
    });


/**
* @swagger
* /api/cognito/RegisterUser:
*   post:
*      summary: This api used for Cognito user Register
*      description: This api used for Cognito user Register
*      requestBody:
*           required: true
*           content:
*                application/json:
*                     schema:
*                          $ref: '#components/schema/RegisterUser'
*      responses:
*          200:
*              description: Registration successful. Please Contact Lounge Administrator for Confirmation     
*/
app.post("/api/cognito/RegisterUser",cors(), (req, res,next) => {
       
        var attributeList = [];
         attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"name",Value:req.body.name}));
         
        var params = {
            ClientId:configCognito.ClientId.clientId,
              Username:req.body.username,
            Password:req.body.password,
           UserAttributes:attributeList
          };

         return new Promise((resolve, reject) => {
            AWS.config.update(configCognito.credential);
            var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
            cognitoidentityserviceprovider.signUp(params, (err, data) => {
                if (err) {
                    // console.log(err);
                    //res.send(err);
                    res.send(resResult(0,400,"Register User not successfully",err,"/api/cognito/RegisterUser"));
                }
                else {
                    // console.log(data);
                   // res.send(data)
                    res.send(resResult(1,200,"Register User successfully",data,"/api/cognito/RegisterUser"));
                }
            })
          }); 

      
        });

        
 
/**
* @swagger
* /api/cognito/updateUser:
*   post:
*      summary: This api used for Cognito user Details Update
*      description: This api used for Cognito user Details Update
*      requestBody:
*           required: true
*           content:
*                application/json:
*                     schema:
*                          $ref: '#components/schema/updateUser'
*      responses:
*          200:
*              description: User Details Update successful    
*/
app.post("/api/cognito/updateUser1",cors(), (req, res,next) => {
       
        var attributeList = [];
         attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"name",Value:req.body.name}));
         attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:portlocation",Value:req.body.portlocation}));
         attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:emailid",Value:req.body.emailid}));
         attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:empid",Value:req.body.empid}));
         attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:dob",Value:req.body.dob}));
         attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:userroles",Value:req.body.role}));
         attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:approvedby",Value:req.body.approvedby}));
        
         var params = {          
            UserPoolId:configCognito.UserPoolId.userPoolId,
            Username:req.body.username,
            UserAttributes:attributeList,               
          };

         return new Promise((resolve, reject) => {
            AWS.config.update(configCognito.credential);
            var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
            cognitoidentityserviceprovider.adminUpdateUserAttributes(params, (err, data) => {
                if (err) {
                    // console.log('aaa1',err);
                    res.send(err)
                }
                else {
                    // console.log('aaa2',data);
                    res.send(data)
                }
            })
          }); 

             
        });

        app.post("/api/cognito/updateUser",cors(), (req, res,next) => {
       
          var attributeList = [];
           attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"name",Value:req.query.name}));
           attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:portlocation",Value:req.query.portlocation}));
           attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:emailid",Value:req.query.emailid}));
           attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:empid",Value:req.query.empid}));
           attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:dob",Value:req.query.dob}));
           attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:userroles",Value:req.query.role}));
           attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:approvedby",Value:req.query.approvedby}));
          
           var params = {          
              UserPoolId:configCognito.UserPoolId.userPoolId,
              Username:req.query.username,
              UserAttributes:attributeList,               
            };
  
           return new Promise((resolve, reject) => {
              AWS.config.update(configCognito.credential);
              var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
              cognitoidentityserviceprovider.adminUpdateUserAttributes(params, (err, data) => {
                  if (err) {
                      // console.log('aaa1',err);
                      //res.send(err)
                      res.send(resResult(0,400,"update User Fail",err,"/api/cognito/updateUser"));
                  }
                  else {
                      // console.log('aaa2',data);
                      //res.send(data)

                      res.send(resResult(1,200,"Update User successfully",data,"/api/cognito/updateUser"));
                  }
              })
            }); 
  
               
          });
        app.post("/api/cognito/updateUserConfirm",cors(), (req, res,next) => {
            debugger;
    

           var params = {
            UserPoolId:configCognito.UserPoolId.userPoolId,
          };
          return new Promise((resolve, reject) => {
            AWS.config.update(configCognito.credential);
            var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
            cognitoidentityserviceprovider.listUsers(params, (err, data) => {
                if (err) {
                    res.send(err)
                }
                else {       
                 
                    let confirmdata=data.Users.filter((item)=>item.UserStatus=== "CONFIRMED" && item.Username===req.query.username);
                   if(confirmdata.length > 0 )
                   {
                    return res.send({statusCode:200,message:'user already exists and Approved'})
                   }
                   let notconfirmdata=data.Users.filter((item)=>item.UserStatus!== "CONFIRMED" && item.Username===req.query.username);
                   
                   if(notconfirmdata.length > 0 )
                   {
                    var params = {
                        UserPoolId: configCognito.UserPoolId.userPoolId, /* required */
                        Username:  req.query.username, /* required */
                       
                      };
                      AWS.config.update(configCognito.credential);
                      var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
                      cognitoidentityserviceprovider.adminConfirmSignUp(params, function(err, data) {
                        if (err)
                        {
                             console.log('err',err, err.stack);
                          return   res.send({statusCode:400,message:'user not approved internal error'});
                         } // an error occurred
                        else  
                        {  
                           apparovedBy(req.query.approvedby,req.query.username) ;
                          return res.send({statusCode:200,message:'user Approved'})
                        }       // successful response
                      });
                    
                   }
                   
                   if(confirmdata.length=== 0 && notconfirmdata.length===0 )
                   {
                    return res.send({statusCode:400,message:'user not found'})
                   }
                  
                 
      
                }
            })
          });   


             
                 
                 
            });
      
async function apparovedBy(approvedby,username)
  {
      console.log('approvedby',approvedby)

      console.log('username',username)

    var attributeList = [];
     attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({Name:"custom:approvedby",Value:approvedby}));
   
    var params = {     
       UserPoolId:configCognito.UserPoolId.userPoolId,
       Username:username,
       UserAttributes:attributeList,
          
     };
        // console.log(JSON.stringify(params));

       // return new Promise((resolve, reject) => {
       AWS.config.update(configCognito.credential);
       var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
       cognitoidentityserviceprovider.adminUpdateUserAttributes(params, (err, data) => {
           if (err) {
                console.log('aaa1',err);
              // res.send(err)
           }
           else {
                console.log('aaa2',data);
              // res.send(data)
           }
       })
    //});
    
  }

/**
 * @swagger
 *  components:
 *      schema:
 *          RegisterUser:
 *                        type: object
 *                        properties:
 *                              id:
 *                                 type:  integer
 *                              username:
 *                                      type: string
 *                              password:
 *                                      type: string
 *                              name:
 *                                  type: string 
 */

  /**
   * @swagger
   * /api/cognito/ValidateToken:
   *   get:
   *      summary: This api used fetch data from Cognito validateToken
   *      description: This api use to check if get method is working or not
   *      responses:
   *          200:
   *              description: this api used fetch data from Cognito validateToken
   *              content:
   *                  application/json:
   *                    schema:
   *                        type: array
   *                        items:
   *                            $ref: '#components/schema/RegisterUser'         
   */
   app.get("/api/cognito/ValidateToken",cors(), (req, res,next) => {
    debugger;
    request({
      url: `https://cognito-idp.${configCognito.Pool_Region.pool_region}.amazonaws.com/${configCognito.poolData.UserPoolId}/.well-known/jwks.json`,
      json: true
  }, function (error, response, body) {
      if (!error && response.statusCode === 200) {
          pems = {};
          var keys = body['keys'];
          for(var i = 0; i < keys.length; i++) {
              //Convert each key to PEM
              var key_id = keys[i].kid;
              var modulus = keys[i].n;
              var exponent = keys[i].e;
              var key_type = keys[i].kty;
              var jwk = { kty: key_type, n: modulus, e: exponent};
              var pem = jwkToPem(jwk);
              pems[key_id] = pem;
          }
          //validate the token
          var decodedJwt = jwt.decode(token, {complete: true});
          if (!decodedJwt) {
              // console.log("Not a valid JWT token");
              return;
          }

          var kid = decodedJwt.header.kid;
          var pem = pems[kid];
          if (!pem) {
              // console.log('Invalid token');
              return;
          }

          jwt.verify(token, pem, function(err, payload) {
              if(err) {
                  // console.log("Invalid Token.");
              } else {
                  // console.log("Valid Token.");
                  // console.log(payload);
              }
          });
      } else {
          // console.log("Error! Unable to download JWKs");
      }
  });
});



    // ChangePassword
  app.post("/api/cognito/ChangePassword",cors(), (req, res,next) => {
      debugger;
      
      var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
        Username: req.body.username,
        Password: req.body.oldpassword,
    });
    const userPool = new AmazonCognitoIdentity.CognitoUserPool(configCognito.poolData);
    var userData = {
        Username: req.body.username,
        Pool: userPool
    };
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

    cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: function (result) {
            cognitoUser.changePassword(req.body.oldpassword, req.body.newpassword, (err, result) => {
                if (err) {
                    // console.log(err);
                    res.send(resResult(0,400,"Change Password Fail",err,"/api/cognito/ChangePassword"))
                } else {
                  debugger;                 
                    // console.log("Successfully changed password of the user.");
                    res.send(resResult(1,200,"Change Password successfully",result,"/api/cognito/ChangePassword"));
                }
            });
        },
        onFailure: function (err) {
            // console.log(err);
            res.send(err)
        },
    });

    
      });
   
    function resResult(status,statusCode,message,result,tag)
    {
      debugger;
      let obj={
        "statusCode":statusCode,
        "input":{},
        "tag":tag,
        "status":status, // success
        "description":message,
        "result":result
        
        }
      return obj;
    }
    // Forget Password
  app.post("/api/cognito/ForgetPassword",cors(), (req, res,next) => {
    debugger;
    
    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
      Username: req.body.username,
      Password: req.body.oldpassword,
  });
  const userPool = new AmazonCognitoIdentity.CognitoUserPool(configCognito.poolData);
  var userData = {
      Username: req.body.username,
      Pool: userPool
  };
  var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

  cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function (result) {
          cognitoUser.forgotPassword(req.body.oldpassword, req.body.newpassword, (err, result) => {
              if (err) {
                  // console.log(err);
                  res.send(resResult(0,400,"Forget Password successfully",err,"/api/cognito/ForgetPassword"))
              } else {
                  // console.log("Successfully Forget password of the user.");
                  res.send(resResult(1,200,"Forget Password successfully",result,"/api/cognito/ForgetPassword"));
              }
          });
      },
      onFailure: function (err) {
          // console.log(err);
          res.send(err)
      },
  });

  
    });
 

        // CONFIRMED User
        app.post("/api/cognito/resendConfirmationCode",cors(), (req, res,next) => {
            debugger;
            
               var params = {
                ClientId:configCognito.ClientId.clientId,
                Username:req.query.username               
                 };
    
             return new Promise((resolve, reject) => {
                AWS.config.update(configCognito.credential);
                var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
                cognitoidentityserviceprovider.resendConfirmationCode(params, (err, data) => {
                    if (err) {
                        // console.log(err);
                        res.send(err)
                    }
                    else {
                        // console.log(data);
                        res.send(data)
                    }
                })
              }); 
          
            });

    app.post("/api/cognito/ConfirmedUser1",cors(), (req, res,next) => {
        debugger;
        
           var params = {
            ClientId:configCognito.ClientId.clientId,
            Username:req.query.username,
            ConfirmationCode:req.query.ConfirmationCode
             };

         return new Promise((resolve, reject) => {
            AWS.config.update(configCognito.credential);
            var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
            cognitoidentityserviceprovider.confirmSignUp(params, (err, data) => {
                if (err) {
                    // console.log(err);
                    res.send(err)
                }
                else {
                    // console.log(data);
                    res.send(data)
                }
            })
          }); 
      
        });

        // CONFIRMED User
  app.post("/api/cognito/ConfirmedUser",cors(), (req, res,next) => {
    debugger;
    
    var attributeList = [];
   
    attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({
        UserStatus: "CONFIRMED",
        Value: "CONFIRMED"
    }));
   
   
   // const userPool = new AmazonCognitoIdentity.CognitoUserPool(configCognito.poolData);
    var userData = {
        Username: req.query.username,
        Pool:configCognito.UserPoolId.userPoolId,
    };
    console.log(userData);
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

    cognitoUser.updateAttributes(attributeList, (err, result) => {
        if (err) {
            //handle error\
            // console.log(err);
            return res.send(err)
        } else {
            // console.log(result);
            return res.send(result)
        }
    });
     
    });
  
    
    const amexGlobal={
      api:{
        ApiKey :"Xf3G6Hqsnf3K1xy559L1yabuT8QKAKEY",
        Secret : "BnKbzT5xA6hw1GCDdl7UUW7UT63F8bDE",
        CertificateFullPath : "D:\\Plaza\\AmexGlobalCode\\amexqa.plaza-network.com.pfx",
        CertificatePath : "\\amexqa.plaza-network.com.pfx",
        CertificatePassWord : "Pl@z@1012",
        Url : "https://api.qa2s.americanexpress.com/feebasedsrvcs/v2/benefits/benefit_families/47d79d0d-87c6-4773-a2d0-1440988f6ca2/usage_approvals",
        CardNo : "371756099791004" ,
        AbsolutePath :"/feebasedsrvcs/v2/benefits/benefit_families/47d79d0d-87c6-4773-a2d0-1440988f6ca2/usage_approvals",
        Host:"api.qa2s.americanexpress.com",
        Port: "443"
      },
      AuthHeaderNames:{
        X_AMEX_API_KEY:"x-amex-api-key",
        X_AMEX_REQUEST_ID :"x-amex-request-id"
      },
      AUTHORIZATION:"Authorization"
  }
 app.post("/api/AmexGlobalCode",cors(), (req, res,next) => {
      
  debugger;
  var result=null;
    result= checkQueryEntitlement(res,amexGlobal.api.ApiKey, amexGlobal.api.Secret, amexGlobal.api.CertificatePath, amexGlobal.api.CertificatePassWord, amexGlobal.api.Url, amexGlobal.api.CardNo);
      
if(result!==null)
{
  res.send(
    {
        statusCode:200,
        result
    });
}
else
{
  res.send(
    {
        statusCode:400,
        err
    });
}
      
      
  
  });

  function checkQueryEntitlement(res,endPoint_ApiKey, endPoint_Secret, endPoint_CertificatePath, endPoint_CertificatePassWord, url, CardNo)
  {

    parameter={
      card :{
        number:CardNo
      }
    };
  console.log('parameter',parameter)
    var headers = GenerateAuthHeaders(endPoint_ApiKey, endPoint_Secret, parameter, url);

    var response = HttpPost(res,headers, endPoint_ApiKey, endPoint_Secret, endPoint_CertificatePath, endPoint_CertificatePassWord, url, parameter);

    if (response===null || response==='')
    {
        result.ErrorMsg = "Internal Error. Please switch to Manual Admission.";
    }
    else
    {

       debugger;
        if (response.error_code!==null)
        {
            result.ErrorCode = usageApprovalResource.error_code;
            result.ErrorMsg = usageApprovalResource.user_message;
        }
        else
        {
            if (result.code.ToUpper() === "APPROVED")
            {
                result.IsEligibility = true;
                result.Benefit = benefit;
                result.Issuer = context.cardProduct.cardIssuer;
                result.LineofBusiness = context.cardProduct.lineOfBusiness;
                result.Market = context.cardProduct.cardMarket;
                result.Product = context.cardProduct.cardProductType;
                result.CustomerType = context.cardProduct.customerType;
            }
            else
            {
                result.ErrorCode = result.code;
                result.ErrorMsg = result.message;
            }
        }
        
    }
    
  }


    //  DeleteUser user 
  app.get("/api/cognito/DeleteUser",cors(), (req, res,next) => {
    debugger;
    const userPool = new AmazonCognitoIdentity.CognitoUserPool(configCognito.poolData);
    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
      Username: req.body.username,
      Password: req.body.password,
  });
  var userData = {
      Username: req.body.username,
      Pool: userPool
  };
  var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
  cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function (result) {
          // console.log(result);
          cognitoUser.deleteUser((err, result) => {
              if (err) {
                res.send(err);
              } else {
                  // console.log("Successfully deleted the user.");
                  res.send(result);
              }
          });
      },
      onFailure: function (err) {
        res.send(err);
      },
  });

  
    });



    app.use(express.static(path.join(__dirname,'dist/cognitologin'))); // node server folder as static
   
   

    //call app.listen(port,host);
    app.use('/',function(req,res){
      console.log(path);
     res.sendFile(path.join(__dirname+'dist/cognitologin/index.html'));
    })
  
app.listen(port1, () => 
 console.log(`Example app updated listening at http://localhost:${port1}`));