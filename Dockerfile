# create docker container
# docker build -d node-docker-image .
# docker image run command
# docker run -it -p 9000:3200 node-docker-image
FROM node:18
WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install && npm cache clean --force 
RUN npm install amazon-cognito-identity-js
COPY . .

EXPOSE 3200
CMD [ "node", "app.js" ]


